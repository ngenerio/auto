class FB {
    constructor (app) {
        this.app = app;
    }

    _share (message, user, time) {
        if (time === 'now' || time === '') {
            this.app.logger.info(`${message} was posted by ${user.getId()}`);
            return null;
        }

        // This in the real story will be handled by some cron job that will check the details of the time
        var timeDiff = new Date(time) - Date.now();
        setTimeout(() => {
            this.app.logger.info(`${message} was posted by ${user.getId()}`);
        }, timeDiff);
    }

    autoShare (user, body) {
        if (!body.time) {
            this._share(body.message, user, user.getSettings() + Date.now());
            return;      
        }

        this._share(body.message, user, body.time);
    }
}


module.exports = function (app) {
    return new FB(app);
};