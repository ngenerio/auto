class User {
    constructor (id, intervalSettings) {
        this._id = id;
        this._intervalSettings = intervalSettings;
    }

    getSettings () {
        return this._intervalSettings;
    }

    getId () {
        return this._id;
    }
}

var Users = [];

for (var i = 0; i < 10; i++) {
    Users.push(new User(i, (i + 1) * 3000));
}

module.exports = {
    getUserById (id) {
        return Users.find((user) => {
            return user.getId() === id; 
        });
    },

    getAllUsers () {
        return Users;
    }
};