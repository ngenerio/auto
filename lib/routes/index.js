const express = require('express');
const basicAuth = require('basic-auth');

function setUpAPIRoutes (app) {
    var router = express.Router();
    
    router.use(function (req, res, next) {
        var reqAuthDetails = basicAuth(req);
        app.logger.info('User details from the server ', reqAuthDetails);

        if (reqAuthDetails.name === app.config.appUsername && reqAuthDetails.pass === app.config.appPassword) {
            return next();
        }

        app.logger.info('Invalid authentication details');
        return res.status(401).json({ status: 'error', message: 'Invalid authentication details'});
    });

    router.post('/fb', function (req, res) {
        var requestDetails = req.body;
        app.logger.info('Request details from the client ', requestDetails);

        var user = app.db.getUserById(requestDetails.userId);
        if (!user) {
            return res.status(404).json({ status: 'error', message: 'User could not be found' });
        }

        app.fb.autoShare(user, requestDetails);
        return res.status(200).json({ status: 'success', message: 'Message has been scheduled to be posted'});
    });

    return router;
}

module.exports = function (app) {
    return {
        api: setUpAPIRoutes(app)
    };
};