const express = require('express');
const bodyParser = require('body-parser');
const log4js = require('log4js');
const dotenv = require('dotenv');
const fb = require('./lib/fb');
const db = require('./lib/models/users');
const routes = require('./lib/routes');

dotenv.config({ path: './.env' });

var Application = {
    server: null,
    logger: null,
    db: null,
    fb: null,
    config: {}
};

Application.start = function () {
    Application.config.appUsername = process.env.API_USERNAME;
    Application.config.appPassword = process.env.API_PASSWORD;
    Application.config.port = process.env.PORT;
    Application.db = db;
    Application.fb = fb(Application);
    Application.logger = log4js.getLogger();
    Application.server = express();
    Application.server.use(bodyParser.urlencoded({ extended: false }));
    Application.server.use(bodyParser.json());
    Application.server.use('/api/v1', routes(Application).api);

    Application.server.listen(Application.config.port, function () {
        Application.logger.info('app server running at ', Application.config.port);
        Application.logger.info(Application.db.getAllUsers());
    });
};

Application.start();